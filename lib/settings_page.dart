import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:prodigee/utils/globals.dart';
import 'package:prodigee/utils/styles.dart';
import 'package:prodigee/widgets/input.dart';

class SettingsPage extends StatefulWidget{
  State<StatefulWidget> createState(){
    return SettingsPageState();
  }
}

class SettingsPageState extends State<SettingsPage>{

  var _proflieForm = GlobalKey<FormState>();
  TextEditingController txtFullname = TextEditingController();
  TextEditingController txtUsername = TextEditingController();
  TextEditingController txtDob = TextEditingController();
  TextEditingController txtPhone = TextEditingController();
  TextEditingController txtEmail = TextEditingController();
  TextEditingController txtGender = TextEditingController();

  FocusNode fnameFocus = FocusNode();
  FocusNode unameFocus = FocusNode();
  FocusNode dobFocus = FocusNode();
  FocusNode phoneFocus = FocusNode();
  FocusNode emailFocus = FocusNode();
  FocusNode genderFocus = FocusNode();
  String? gender;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.white,
        primarySwatch: Colors.grey
      ),
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            'Settings',
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {  },
          ),
          actions: [
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {  },
            ),
          ],
        ),
        body: Form(
            key: _proflieForm,
            child: ListView(
              children: [

                Stack(
                  alignment: Alignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.all(20),
                      child: Container(
                        width: 120,
                        height: 120,
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          shape: BoxShape.circle,
                        ),
                      ),
                    ),
                    Positioned(
                        bottom: 10,
                        right: 100,
                        child: Align(
                          alignment: Alignment.bottomRight,
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.black87,
                              shape: BoxShape.circle,
                            ),
                            child: IconButton(
                              icon: Icon(Icons.edit,color: Colors.white,),
                              onPressed: () {  },
                            ),
                          ),
                        )
                    )
                  ],
                ),

                wTxtFormField(
                  label: 'Full name',
                  txtInputType: TextInputType.text,
                  txtEditingController: txtFullname,
                  txtFocus: fnameFocus,
                  txtNextFocus: unameFocus,
                  txtminLength: 2,
                  obscureTxt: false,
                ),

                wTxtFormField(
                  label: 'Username',
                  txtInputType: TextInputType.text,
                  txtEditingController: txtUsername,
                  txtFocus: unameFocus,
                  txtNextFocus: emailFocus,
                  txtminLength: 2,
                  obscureTxt: false,
                ),

                wTxtFormField(
                  label: 'Email',
                  txtInputType: TextInputType.emailAddress,
                  txtEditingController: txtEmail,
                  txtFocus: emailFocus,
                  txtNextFocus: phoneFocus,
                  txtminLength: 2,
                  obscureTxt: false,
                ),

                wTxtFormField(
                  label: 'Phone Number',
                  txtInputType: TextInputType.phone,
                  txtEditingController: txtPhone,
                  txtFocus: phoneFocus,
                  txtNextFocus: dobFocus,
                  txtminLength: 10,
                  obscureTxt: false,
                ),

                Padding(
                  padding: const EdgeInsets.only(left: 20,top: 20),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Date of Birth',),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10,left: 20, right: 20),
                  child:  TextFormField(
                    obscureText: false,
                    focusNode: dobFocus,
                    controller: txtDob,
                    textInputAction: TextInputAction.next,
                    validator: (String? value){
                      if(value!.trim().isEmpty){
                        return "DOB is required!";
                      }else if(txtDob.text.length < 10){
                        return "Invalid DOB";
                      }return null;
                    },
                    onFieldSubmitted: (term){
                      FocusScope.of(context).requestFocus(genderFocus);
                    },
                    decoration: sInputOutline('Date of Birth'),
                    onTap: (){
                      DatePicker.showDatePicker(context,
                          showTitleActions: true,
                          minTime: DateTime(1940, 3, 5),
                          maxTime: DateTime(2004, 6, 7),
                          theme: DatePickerTheme(
                              headerColor: Colors.grey,
                              backgroundColor: Colors.white,
                              itemStyle: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18),
                              doneStyle:
                              TextStyle(color: Colors.white, fontSize: 16)),
                          onChanged: (date) {
                            print('change $date in time zone ' +
                                date.timeZoneOffset.inHours.toString());
                          }, onConfirm: (date) {
                            print('confirm $date');
                            txtDob.text = date.toString().substring(0, 10);
                          }, currentTime: DateTime.now(), locale: LocaleType.en);
                    },
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(left: 20,top: 20),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Gender',),
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.only(left: 20,right: 20,top: 10),
                    child: DropdownButtonFormField(
                      decoration: sInputOutline('Gender'),
                      focusNode: genderFocus,
                      items: Globals.genderData.map((item) {
                        return DropdownMenuItem(
                          child: Text(item['name'],),
                          value: item['uri'].toString(),
                        );
                      }).toList(),
                      validator: (value) {
                        if (value == null) {
                          return 'Bank Name is required';
                        }
                      },
                      onChanged: (value) {
                        gender = value.toString();
                      },
                      isExpanded: true,
                    )
                ),

                Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: TextButton(
                          style: TextButton.styleFrom(
                            padding: const EdgeInsets.all(16.0),
                            primary: Colors.black54,
                            backgroundColor: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12),
                            ),
                          ),
                          onPressed: () {  },
                          child: Text(
                            'Cancel',
                          ),
                        ),
                      )

                    ),Expanded(
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: TextButton(
                          style: TextButton.styleFrom(
                            padding: const EdgeInsets.all(16.0),
                            primary: Colors.black54,
                            backgroundColor: Colors.black54,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12),
                            ),
                          ),
                          onPressed: () { doSaveProfile(); },
                          child: Text(
                            'Save',style: TextStyle(color: Colors.white),
                          ),
                        ),
                      )

                    ),

                  ],
                )

              ],
            ),
        ),

      ),
    );
  }


  Future doSaveProfile() async {
    if(_proflieForm.currentState!.validate()){
      try {
        final result = await InternetAddress.lookup('google.com');
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
          Fluttertoast.showToast(
              msg: "Good to go",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );
        }
      } on SocketException catch (_) {
        Fluttertoast.showToast(
            msg: "Network Issue",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }

    }
  }

}