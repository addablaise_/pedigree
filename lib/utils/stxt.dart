import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';



stxtFree(txtColor,txtSize,weight){
  String? fam;
  if (weight == 'bold'){
    fam = 'Inter-Bold';
  }else if (weight == 'med'){
    fam = 'Inter-Medium';
  }else if (weight == 'reg'){
    fam = 'Inter-Regular';
  }
  return TextStyle(
    color: txtColor,
    fontSize: txtSize,
    fontFamily: fam,
  );
}


