import 'package:flutter/material.dart';

sInputOutline(label){
  return InputDecoration(
    labelText: label,
    floatingLabelBehavior: FloatingLabelBehavior.never,
    contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
    border: OutlineInputBorder(
      borderSide: BorderSide(color: Colors.blueGrey,width: 1.5),
      borderRadius: BorderRadius.circular(12),
    ),
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Colors.black54,width: 2),
      borderRadius: BorderRadius.circular(12),
    ),
    focusedBorder:OutlineInputBorder(
      borderSide: BorderSide(color: Colors.black54, width: 2.0),
      borderRadius: BorderRadius.circular(12.0),
    ),
    fillColor: Colors.white.withOpacity(0.8),
    filled: true,
  );
}
