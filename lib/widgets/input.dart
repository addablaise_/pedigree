import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:prodigee/utils/stxt.dart';
import 'package:prodigee/utils/styles.dart';

class wTxtFormField extends StatelessWidget {
  final String? label,hint;
  final TextStyle? txtStyle;
  final TextInputType txtInputType;
  final TextEditingController txtEditingController;
  final FocusNode txtFocus;
  final int txtminLength;
  final FocusNode? txtNextFocus;
  final bool obscureTxt;
  final bool? isEnabled;
  int? txtmaxLength;
  wTxtFormField(
      {this.onTap,required this.label,this.hint,this.txtStyle,required this.txtInputType,
        required this.txtEditingController,required this.txtFocus,this.txtNextFocus,
        required this.txtminLength,required this.obscureTxt,this.txtmaxLength,this.isEnabled});
  GestureTapCallback? onTap;
  @override
  Widget build(BuildContext context) {
    getMax(){
      if(txtInputType == TextInputType.multiline){
        return null;
      }else{
        return 1;
      }
    }
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 20,top: 20),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              label!,
              style: stxtFree(Color(0xFF6B7B8A),14.0,'med'),),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20,right: 20,top: 10),
          child: TextFormField(
            keyboardType: txtInputType,
            style: txtStyle,
            obscureText: obscureTxt,
            focusNode: txtFocus,
            controller: txtEditingController,
            textInputAction: TextInputAction.next,
            maxLines: getMax(),
            textCapitalization: TextCapitalization.sentences,
            onTap: onTap,
            enabled: isEnabled,
            validator: (String? value){
              if(value!.trim().isEmpty){
                return label!+ " is required!";
              }else if(txtEditingController.text.length < txtminLength){
                return "Invalid " +label!+' - '+txtminLength.toString()+' or more characters';
              }else if(txtInputType == TextInputType.emailAddress){

                if(value.trim().isNotEmpty){
                  bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value);
                  if(!emailValid){
                    return "Invalid Email!";
                  }
                }
              }else if(txtInputType == TextInputType.phone){
                if (value.isEmpty) {
                  return 'Please enter mobile number';
                }
                bool phoneValid = RegExp(r"(^(?:[+0]2)?[0-9]{10,12}$)").hasMatch(value);
                if (!phoneValid) {
                  return 'Please enter valid mobile number';
                }
              }else if(txtInputType == TextInputType.number){
                if (value.isEmpty) {
                  return 'Please enter number';
                }
                bool numValid = RegExp(r"(^(?:[0]2)?[0-9]{1,5}$)").hasMatch(value);
                if (!numValid) {
                  return 'Please enter valid number';
                }
              }return null;
            },
            onFieldSubmitted: (term){
              FocusScope.of(context).requestFocus(txtNextFocus);
            },
            decoration: sInputOutline(label),
          ),
        ),

      ],
    );
  }
}